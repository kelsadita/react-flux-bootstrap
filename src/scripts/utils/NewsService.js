import BreakingNewsActions from '../actions/BreakingNewsActions';
import $ from 'jquery';

const NewsService = {
  getBreakingNewsList () {
    var latestNews =  ["helllo world!", "test1", "test2"];

    $.ajax({
      url: 'http://localhost:3000/news/breakingnews',
      success: function (data) {
        console.log(data);
      },
      error: function (data) {
        console.log(data);
      }
    });

    BreakingNewsActions.retrieveNews(latestNews);
  }
}

export default NewsService;
