import React from 'react';
import Header from '../components/templates/header'
import Footer from '../components/templates/footer'
import BreakingNewsStore from '../stores/BreakingNewsStore';
import NewsService from '../utils/NewsService';

function getStateFromStores() {
  return {
    news: BreakingNewsStore.getAll()
  }
}


const App = React.createClass({

  getInitialState () {
    return getStateFromStores();
  },

  componentDidMount () {
    BreakingNewsStore.addChangeListener(this._onChange);
  },

  fetchBreakingNews () {
    NewsService.getBreakingNewsList();
  },

  render () {
    return (
      <div>
        <Header />
        <article className="context">
          <h1 onClick={this.fetchBreakingNews}>NewsList</h1>
          <ul>
            {this.state.news.map((newsItem) => (<li>{newsItem}</li>))}
          </ul>
        </article>
        <Footer />
      </div>
    );
  },

  _onChange () {
    this.setState(getStateFromStores());
  }
});

export default App;
