import React from 'react';

const Footer = React.createClass({
  render () {
    return (
      <div>
        <footer>
          <hr/>
          <p>Kelsaditas Production</p>
        </footer>
      </div>
    );
  }
});

export default Footer;

